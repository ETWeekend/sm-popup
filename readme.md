# Popup

Lightweight JavaScript popup library

## Installation and Usage

Install the library with `npm install @southmedia/popup` or  `yarn @southmedia/popup`

#### ES6

```javascript
import Popup from "@southmedia/popup"
```

## Example

```javascript
new Popup({
  popupSelector: "[data-popup-example]",
  triggerSelector: "[data-popup-example-trigger]",
  offsetElementsSelector: "[data-offset-popup]",
  beforeOpen: (event) => {
    console.log("before open", event.detail);
    const popup = event.detail.popup;
    const trigger = event.detail.trigger;
  },
  afterOpen: (event) => {
    console.log("after open", event.detail);
  },
  beforeClose: (event) => {
    console.log("before close", event.detail);
  },
  afterClose: (event) => {
    console.log("after close", event.detail);
  }
})
```

## Options

Name | Type | Default | Description |
------------- | ------------- | ------------- | ------------- |
activeClass | string | "opened" | Opened popup active class
bodyClass | string | - | Body class then popup opened
popupSelector | string | - |  Popup selector. Required
triggerSelector | string | - | Element selector click on will open popup
closeSelector | string | "[data-popup-close]" | Element selector click on will close popup
lockScroll | boolean | true | If enabled: lock scroll on window and add margin by scrollbar width to prevent shaking
offsetElementsSelector | string | - | Element selector to add right by scrollbar width. Useless when lockScroll: false.
animationDuration | number | 600 | Transition duration (in ms)
animationManual | boolean | false | If enabled: animation will need to be set manually

## Events

Name  | Description
------------- | -------------
beforeOpen  | Event will be fired right before opened
afterOpen  | Event will be fired right after opened
beforeClose  | Event will be fired right before closed
afterClose  | Event will be fired right after closed

Events have an argument "event", that contains popup and trigger that fired popup.
```javascript
event.detail = {
  popup: element,
  trigger: element
}
```

## Maintainers

* [achoplyak](https://git.dev.southmedia.ru/achoplyak) - Andrey Choplyak

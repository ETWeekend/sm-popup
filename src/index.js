class Popup {
  defaultOptions = {
    activeClass: "opened",
    /** @param {string} activeClass - класс для активного модального окна */
    bodyClass: null,
    /** @param {string} bodyClass - класс для body когда открыто модальное окно */
    popupSelector: null,
    /** @param {string} popupSelector - селектор для модального окна */
    triggerSelector: null,
    /** @param {string} triggerSelector - селектор для элемента, при клике на который будет открываться модальное окно */
    closeSelector: "[data-popup-close]",
    /** @param {string} closeSelector - селектор для элемента, при клике на который будет закрываться модальное окно */
    lockScroll: true,
    /** @param {boolean} lockScroll - определяет нужно ли блокировать скролл страницы при открытии модального окна */
    offsetElementsSelector: null,
    /** @param {string} offsetElementsSelector - селектор элементов которым необходимо добавить right на ширину скроллбара при lockScroll: true */
    animationDuration: 600,
    /** @param {number} animationDuration - время анимации в ms */
    animationManual: false,
    /** @param {boolean} animationManual - false: анимация по умолчанию, true: анимацию нужно задавать в ручном режиме */
  }

  constructor(options) {
    this.$options = Object.assign(this.defaultOptions, options);

    if (!this.$options.popupSelector) return console.error("[Popup]: не передан popupSelector");
    this.$popup = document.querySelector(this.$options.popupSelector);
    if (!this.$popup) return console.error(`[Popup]: не найден ${this.$options.popupSelector}`);
    this.$animationPlaying = false; // статус анимации для игнорирования open/close пока анимация не закончится
    window.isScrollLocked = false; // статус - запрещен ли скролл страницы

    this.$offsetElements = [];
    if (this.$options.offsetElementsSelector) {
      this.$offsetElements = document.querySelectorAll(this.$options.offsetElementsSelector);
    }

    this.setHandlers();
  }

  /**
   * Устанавливаем слушатели событий кликов, esc, для открытия/закрытия модального окна
   */
  setHandlers() {
    if (this.$options.triggerSelector) {
      document.addEventListener("click", (event) => {
        const trigger = event.target.closest(this.$options.triggerSelector);
        if (trigger) this.openPopup(trigger);
      });
    }

    const closes = this.$popup.querySelectorAll(this.$options.closeSelector);
    closes.forEach(close => close.addEventListener("click", () => this.closePopup()));

    document.addEventListener("keyup", (event) => {
      if (event.key === "Escape" || event.key === "Esc") {
        if (this.$popup.classList.contains(this.$options.activeClass)) this.closePopup();
      }
    });

    if (this.$options.beforeOpen !== undefined) {
      this.$popup.addEventListener("beforeOpen", this.$options.beforeOpen.bind(this));
    }

    if (this.$options.afterOpen !== undefined) {
      this.$popup.addEventListener("afterOpen", this.$options.afterOpen.bind(this));
    }

    if (this.$options.beforeClose !== undefined) {
      this.$popup.addEventListener("beforeClose", this.$options.beforeClose.bind(this));
    }

    if (this.$options.afterClose !== undefined) {
      this.$popup.addEventListener("afterClose", this.$options.afterClose.bind(this));
    }
  }

  /**
   * Открытие модального окна
   * @param {string} trigger - элемент по которому был совершен клик для вызова модального окна
   */
  openPopup(trigger) {
    if (this.$animationPlaying) return;
    this.$popup.dispatchEvent(new CustomEvent("beforeOpen", {
      detail: { popup: this.$popup, trigger: trigger }
    }));

    new Promise(resolve => {
      if (this.$options.animationManual) {
        setTimeout(() => resolve(), this.$options.animationDuration);
      } else {
        this.$animationPlaying = true;

        if (this.$options.lockScroll && !window.isScrollLocked) {
          const scrollBarWidth = window.innerWidth - document.documentElement.clientWidth;
          this.$offsetElements.forEach(element => element.style.right = `${scrollBarWidth}px`);
          document.documentElement.style.marginRight = `${scrollBarWidth}px`;
          document.documentElement.style.overflow = "hidden";
          window.isScrollLocked = true;
        }

        this.$popup.style.setProperty("display", "block", "important");
        this.$popup.animate([
          { "opacity": "0" },
          { "opacity": "1" }
        ], {
          duration: this.$options.animationDuration,
          fill: "forwards",
        }).onfinish = () => {
          resolve();
        };
      }
    }).then(() => {
      this.$popup.classList.add(this.$options.activeClass);
      if (this.$popup.bodyClass) document.body.classList.add(this.$popup.bodyClass);
      this.$animationPlaying = false;

      this.$popup.dispatchEvent(new CustomEvent("afterOpen", {
        detail: { popup: this.$popup, trigger: trigger }
      }));
    });
  }

  /**
   * Закрытие модального окна
   */
  closePopup() {
    if (this.$animationPlaying) return;
    this.$popup.dispatchEvent(new CustomEvent("beforeClose", {
      detail: { popup: this.$popup }
    }));

    new Promise(resolve => {
      if (this.$options.animationManual) {
        setTimeout(() => resolve(), this.$options.animationDuration);
      } else {
        this.$animationPlaying = true;
        this.$popup.animate([
          { "opacity": "1" },
          { "opacity": "0" }
        ], {
          duration: this.$options.animationDuration,
          fill: "forwards",
        }).onfinish = () => {
          this.$popup.style.removeProperty("display");

          if (this.$options.lockScroll && window.isScrollLocked) {
            this.$offsetElements.forEach(element => element.style.removeProperty("right"));
            document.documentElement.style.removeProperty("margin-right");
            document.documentElement.style.removeProperty("overflow");
            window.isScrollLocked = false;
          }

          resolve();
        };
      }
    }).then(() => {
      this.$popup.classList.remove(this.$options.activeClass);
      if (this.$popup.bodyClass) document.body.classList.remove(this.$popup.bodyClass);
      this.$animationPlaying = false;

      this.$popup.dispatchEvent(new CustomEvent("afterClose", {
        detail: { popup: this.$popup }
      }));
    });
  }
}

export default Popup;
